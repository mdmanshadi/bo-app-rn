import React from 'react';
import {View, ScrollView, Dimensions,Image,FlatList,TouchableOpacity,Alert,Modal} from 'react-native';
import {connect, setStore} from "trim-redux";
import {Setting,numberFormat} from "./components/Setting";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";
import Header from "./components/Header";
import {Actions} from "react-native-router-flux";
import TextField from "./components/TextField";
import Box from "./components/Box";
import {Icon} from "native-base";
import FaText from "./components/FaText";
import MyButton from "./components/MyButton";
import ListBox from "./components/ListBox";
import Screen from "./components/Screen";
import MyButtonSelect from "./components/MyButtonSelect";

export default class AllCourses extends React.Component {

    componentWillMount(){
        this.setState({
            courses : [],
            courses1 : [],
            loading:true,
            q:'',
            sort : '',
            show_type : '1',
            sort_type : 'date',
            setting : false,
        },()=>this.loadData());

    }

    loadData =()=>{
        this.setState({loading:true});
        Fetcher("getCourses",{q:this.state.q,sort_type:this.state.sort_type},true)
            .then((res)=>{
                if(res.status == 'OK'){
                    this.setState({
                        courses : res.data,
                        courses1 : res.data,
                        loading:false,
                    })
                }else{
                    Actions.reset("splash");
                }
            });
    }
    open(item){
        if(item.type == 'course')
            return Actions.Course({id:item.rel_id});
        if(item.type == 'package')
            return Actions.Package({id:item.rel_id});
        if(item.type == 'Lesson')
            return Actions.Lesson({id:item.rel_id});
    }
    render() {
        var {height, width} = Dimensions.get('window');
        return(
            <Screen   title={"همه دروس"}>
                <Loading loading={this.state.loading}/>

                <Modal
                    animationType="slide"
                    transparent={true}
                    onRequestClose={() => {}}
                    visible={this.state.setting}
                >
                    <View style={{justifyContent:'center', alignItems:'center',backgroundColor:'rgba(0,0,0,0.5)',flex:1}}>
                        <View style={{width:'80%', height:'70%',padding:15, backgroundColor:Setting.primaryColor,borderRadius:Setting.borderRadius,borderColor:'#ccc'}}>
                            <View style={{backgroundColor:Setting.headerColor,justifyContent:'center',alignItems:'center',borderRadius:Setting.borderRadius,padding:5}}>
                                <FaText style={{color:'#fff'}}>تنظیمات نمایش</FaText>
                            </View>
                            <View style={{marginTop:8}}>
                                <FaText style={{borderTopColor:'#000',borderTopWidth:1,padding:8,color:'#fff'}}>نوع نمایش</FaText>
                                <View style={{justifyContent:'center', alignItems:'center',flexDirection:'row-reverse',marginTop:15,padding:8,backgroundColor:'#fff',borderRadius:5}}>
                                    <MyButtonSelect onPress={()=>this.setState({show_type:'1'})} style={{width:'40%',margin:10}} checked={(this.state.show_type == '1')}>لیستی</MyButtonSelect>
                                    <MyButtonSelect slim onPress={()=>this.setState({show_type:'2'})} style={{width:'40%',margin:10}} checked={(this.state.show_type == '2')}>پنجره ای</MyButtonSelect>
                                </View>
                                <FaText style={{borderTopColor:'#000',borderTopWidth:1,padding:8,color:'#fff'}}>مرتب سازی بر اساس</FaText>
                                <View style={{justifyContent:'flex-end',alignItems:'flex-end',marginTop:8,padding:8,backgroundColor:'#fff',borderRadius:5}}>
                                    <MyButtonSelect style={{marginTop:10}} onPress={()=>this.setState({sort_type:'date'})}  checked={(this.state.sort_type == 'date')}>جدیدترین ها</MyButtonSelect>
                                    <MyButtonSelect style={{marginTop:10}} onPress={()=>this.setState({sort_type:'pricelowest'})}  checked={(this.state.sort_type == 'pricelowest')}>قیمت از کم به زیاد</MyButtonSelect>
                                    <MyButtonSelect style={{marginTop:10}} onPress={()=>this.setState({sort_type:'pricehighest'})} checked={(this.state.sort_type == 'pricehighest')}>قیمت از زیاد به کم</MyButtonSelect>
                                    <MyButtonSelect style={{marginTop:10}} onPress={()=>this.setState({sort_type:'view'})} checked={(this.state.sort_type == 'view')}>پربازدیدترین ها</MyButtonSelect>
                                </View>
                                <MyButton style={{marginTop:15,width:'100%'}} slim onPress={()=>{this.setState({setting:false,courses:[],courses1:[]});setTimeout(()=>this.loadData(),100)}}  bg={Setting.headerColor}>ذخیره تنظیمات</MyButton>


                            </View>
                        </View>
                    </View>
                </Modal>
                <View style={{flexDirection:'row-reverse',justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                    <TextField slim onSubmitEditing={()=>this.loadData()} placeholder={"جستجو"} style={{width:'60%',marginLeft:10,padding:1}} value={this.state.q} onChangeText={(value)=>this.setState({q:value})}/>
                    <TouchableOpacity  activeOpacity={1} style={{marginLeft:10,backgroundColor:Setting.red,borderRadius:10,padding:10}}>
                        <Icon name={'search'} onPress={()=>this.loadData()} type={"FontAwesome"} style={{color:Setting.white,fontSize:11}}></Icon>
                    </TouchableOpacity>
                    <TouchableOpacity  activeOpacity={1}>
                        <Icon name={'cog'} onPress={()=>this.setState({setting:true})} type={"FontAwesome"} style={{fontSize:18}}></Icon>
                    </TouchableOpacity>



                </View>
                {this.state.courses.length > 0 &&
                this.state.show_type == '1' ?
                    <FlatList
                        style={[Setting.flatListStyle, {width: '100%'}]}
                        contentContainerStyle={{
                            justifyContent: 'center',
                            alignContent: 'center',
                            alignItems: 'center',
                            width: '100%'
                        }}
                        data={this.state.courses1}
                        extraData={this.state}
                        renderItem={({item}) => <ListBox action package={(item.type == 'package')} item={item}/>}
                        numColumns={1}
                        keyExtractor={(item) => item.id+item.title}
                    />
                    :
                    <FlatList
                        style={[Setting.flatListStyle, {width: '100%'}]}
                        contentContainerStyle={{
                            justifyContent: 'center',
                            alignContent: 'center',
                            alignItems: 'center',
                            width: '100%'
                        }}
                        key={1}
                        data={this.state.courses}
                        extraData={this.state}
                        renderItem={({item}) => <Box action  package={(item.type == 'package')} item={item}/>}
                        numColumns={2}
                        keyExtractor={(item) => item.id}
                    />

                }
                { this.state.courses.length == 0 && !this.state.loading &&
                    <View style={{justifyContent:'center',alignItems:'center'}}><FaText>هیچ محصولی موجود نیست</FaText></View>
                }



            </Screen>
        )

    }

}
