import React from 'react';
import {View, ScrollView, Dimensions,Image,FlatList,TouchableOpacity,Alert} from 'react-native';
import {connect, getStore} from "trim-redux";
import FaText from "./components/FaText";
import {Setting,numberFormat} from "./components/Setting";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";
import MyButton from "./components/MyButton";
import {Actions} from "react-native-router-flux";
import Communications from "react-native-communications";
import TextField from "./components/TextField";
import Screen from "./components/Screen";
import {alertMsg, toastMsg} from "./components/Helper";

class Exam extends React.Component {

    componentWillMount(){
        var type = '';
        if(this.props.typee == 'package')
            type = 'پکیج';
        else if(this.props.typee == 'course')
            type = 'دوره';
        else if(this.props.typee == 'lesson')
            type = 'درس';

        this.setState({
            loading:true,
            data : {},
        });
        this.loadData();

    }
    loadData(){
        Fetcher("getQuizDetails",{id:this.props.id,type:this.props.typee},true)
            .then((res)=>{
                this.setState({
                    loading:false,
                })
                if(res.status == 'OK') {
                    this.setState({
                        data: res.data,
                    })
                }else{
                    toastMsg(res.data);
                }
            });
    }
    startExam(){
        alertMsg("شروع آزمون",getStore('config').exam_start_text + "\n\nآیا مطمین به شروع این آزمون هستید ؟",
            [
                {text: 'لغو', onPress: () => {}},
                {text: 'شروع آزمون', onPress: () => {this.chk()}},
            ],
            {cancelable: false}
        );
    }
    chk(){
        var extra = this.state.data.extra || '';
        if(extra.length > 0){
            alertMsg("پیام آزمون",this.state.data.extra,
                [
                    {text: 'لغو', onPress: () => {}},
                    {text: 'شروع آزمون', onPress: () => {this.chkPayment()}},
                ],
                {cancelable: false}
            );
        }else this.StartIt();
    }
    chkPayment(){
        if(parseInt(this.state.data.payable) > 0){
            if(parseInt(this.state.data.payable) > parseInt(this.props.homeData.user.credit)) {
                alertMsg("هزینه آزمون", "موجودی کیف پول شما برای پرداخت هزینه آزمون کافی نیست . لطفا ابتدا کیف پول خود را شارژ کنید.",
                    [
                        {
                            text: 'لغو', onPress: () => {
                            }
                        },
                        {
                            text: 'شارژ کیف پول', onPress: () => {
                                return Actions.Wallet({amount: this.state.data.payable})
                            }
                        },
                    ],
                    {cancelable: false}
                );
            }else return this.StartIt();
        }
    }
    StartIt(){
        this.setState({
            loading:true,
        });

        Fetcher("startQuiz",{id:this.props.id,type:this.props.typee},true)
            .then((res)=>{
                this.setState({
                    loading:false,
                })
                if(res.status == 'OK') {
                    return Actions.reset("Quiz",{data:res.data});
                }else{
                    setTimeout(()=>{toastMsg(res.data)},100);
                }

            });
    }
    render() {
        var {height, width} = Dimensions.get('window');

        return(
            <Screen title={"شرکت در آزمون"} float >
                <Loading new loading={this.state.loading}/>
                <View style={{marginRight:5,marginLeft:5,marginTop:10,marginBottom:10,justifyContent:'center',alignItems:'center'}}>

                    <Image resizeMode={'stretch'} source={require('./../assets/images/bg/exam.png')} style={{height:width * (329/500),width:width}} />
                    <FaText style={{fontSize:15,color:Setting.red,marginTop:10}}>شرکت در آزمون</FaText>
                    <View style={{margin:8,borderColor:Setting.white,borderWidth:1,padding:10,width:'85%',borderRadius:5,flexDirection:'column',backgroundColor:Setting.white}}>

                        <View  style={{flexDirection:'row-reverse',alignItems:'center',margin:3}}>
                            <FaText>محصول : </FaText>
                            <FaText >{this.state.data.product_name}</FaText>
                        </View>
                        <View  style={{flexDirection:'row-reverse',alignItems:'center',margin:3}}>
                            <FaText>تعداد سوالات : </FaText>
                            <FaText>{this.state.data.total_question} عدد</FaText>
                        </View>
                        <View  style={{flexDirection:'row-reverse',alignItems:'center',margin:3}}>
                            <FaText>امتیاز کل : </FaText>
                            <FaText>{(this.state.data.max_score)} امتیاز</FaText>
                        </View>

                        <View  style={{flexDirection:'row-reverse',alignItems:'center',margin:3}}>
                            <FaText>حداقل امتیاز قبولی : </FaText>
                            <FaText>{(this.state.data.min_score)} امتیاز</FaText>
                        </View>
                        <View  style={{flexDirection:'row-reverse',alignItems:'center',margin:3}}>
                            <FaText>زمان پاسخگویی : </FaText>
                            <FaText>{(this.state.data.exam_time)} دقیقه</FaText>
                        </View>
                        <View  style={{flexDirection:'row-reverse',alignItems:'center',margin:3}}>
                            <FaText>تعداد دفعات مجاز شرکت در آزمون : </FaText>
                            <FaText>{(this.state.data.max_use)} بار</FaText>
                        </View>
                        <View  style={{flexDirection:'row-reverse',alignItems:'center',margin:3}}>
                            <FaText>توضیحات : </FaText>
                            <FaText>{(this.state.data.description)}</FaText>

                        </View>
                        <View style={{width:'100%',justifyContent:'center',alignItems:'center'}}>
                            <FaText>{(this.state.data.extra)}</FaText>
                        </View>

                    </View>
                    <View style={{margin:4,borderColor:Setting.white,borderWidth:1,paddingTop:5,padding:5,width:'85%',borderRadius:5,flexDirection:'column'}}>
                        <MyButton onPress={()=>this.startExam()} style={{marginTop:5}}>شروع آزمون</MyButton>
                    </View>
                </View>
            </Screen>
        )

    }

}
const mapStateToProps = (state) => {
    return {
        homeData : state.homeData

    }
};
export default connect(mapStateToProps)(Exam);

