import React from 'react';
import {View, ScrollView, Dimensions,Image,ImageBackground,FlatList,TouchableOpacity,Alert} from 'react-native';
import {connect, setStore} from "trim-redux";
import FaText from "./components/FaText";
import {Setting,numberFormat} from "./components/Setting";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";

import {Actions} from "react-native-router-flux";
import Screen from "./components/Screen";
import {Icon} from 'native-base';
import {alertMsg, toastMsg} from "./components/Helper";
export default class Favs extends React.Component {

    componentWillMount(){
        this.setState({
            selected : [],
            courses : [],
            loading:true,
            showRemove:false,
        })
        this.loadData();
    }

    loadData(){
        Fetcher("getUserFavCourses",{},true)
            .then((res)=>{
                if(res.status == 'OK'){
                    this.setState({
                        courses : res.data,
                        loading:false,
                    })
                }else{
                    Actions.reset("splash");
                }
            });
    }
    open(item){
        if(item.type == 'category')
            return Actions.replace("Course",{id:item.rel_id});
        if(item.type == 'package')
            return Actions.replace("Package",{id:item.rel_id});
        if(item.type == 'lesson')
            return Actions.replace("Lesson",{id:item.rel_id});
    }
    type(item){
        if(item.type == 'category')
            return "دوره";
        if(item.type == "package")
            return "پکیج";
        if(item.type == 'lesson')
            return "درس";
    }
    render() {
        var {height, width} = Dimensions.get('window');
        if(this.state.courses.length == 0)
            return(
                <Screen title={"علاقه مندی"} float={true} alignItems={'center'}>
                    <Loading new loading={this.state.loading}/>
                    <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:30,marginBottom:20,justifyContent:'center',alignItems:'center'}}>
                        <Image resizeMode={'stretch'} source={require('./../assets/images/bg/favs.png')} style={{height:width/(1372/880),width:width,marginBottom:30}} />

                        {!this.state.loading &&
                            <View style={{backgroundColor:Setting.red,flexDirection:'row-reverse',alignItems:'center',justifyContent:'center',padding:10}}>
                                <Image source={require('../assets/images/drawer/error.png')} style={{width:30,height:30,marginLeft:8}}/>
                                <FaText style={{color:'#fff',fontSize:12}}>دوره ای اضافه نشده است.</FaText>
                            </View>
                        }

                    </View>
                </Screen>
            )
        else
            return(
                <Screen title={"علاقه مندی"} float={true} alignItems={'center'}>
                    <Loading new loading={this.state.loading}/>

                    <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:15,marginBottom:15,justifyContent:'center',alignItems:'center'}}>
                        <ImageBackground resizeMode={'stretch'} source={require('./../assets/images/bg/favs.png')} style={{height:width/(1372/880),width:width,justifyContent:'flex-end',alignItems:'flex-start'}} >
                            {this.state.showRemove &&
                                <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center',margin:10,marginLeft:25}}>
                                    <FaText style={{marginLeft:5}}>انتخاب همه</FaText>
                                    <TouchableOpacity style={{width:15,height:15,borderWidth:1,borderColor:Setting.headerColor,justifyContent:'center',alignItems:'center',marginLeft:10}} onPress={()=>this.select('all')}>
                                        {this.state.selected.length == this.state.courses.length &&
                                        <View style={{
                                            width: 13,
                                            height: 13,
                                            backgroundColor: Setting.headerColor
                                        }}/>
                                        }
                                    </TouchableOpacity>
                                    <FaText style={{marginLeft:5,marginRight:5}}>|</FaText>
                                    <TouchableOpacity onPress={()=>{
                                        this.remove();
                                    }} style={{marginRight:5,marginLeft:0}}>
                                        <Icon style={{fontSize:16}} name={"trash"} />
                                    </TouchableOpacity>
                                </View>
                            }
                        </ImageBackground>
                        <View style={{width:'100%'}}>
                        <FlatList
                            style={Setting.flatListStyle}
                            data={this.state.courses}
                            extraData={this.state}
                            renderItem={({item}) =>{
                                return(
                                    <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center'}}>
                                        {this.state.showRemove &&
                                            <TouchableOpacity style={{width:15,height:15,borderWidth:1,borderColor:Setting.headerColor,justifyContent:'center',alignItems:'center',marginLeft:10}} onPress={()=>this.select(item.id)}>
                                                {this.state.selected.indexOf(item.id) > -1 &&
                                                <View style={{
                                                    width: 13,
                                                    height: 13,
                                                    backgroundColor: Setting.headerColor
                                                }}/>
                                                }
                                            </TouchableOpacity>
                                        }

                                        <TouchableOpacity onLongPress={()=>{this.select(item.id);this.setState({showRemove:true})}}  onPress={()=>this.open(item)} style={{padding:8,width:'80%',borderRadius:Setting.borderRadius,margin:5,flexDirection:'row-reverse',alignItems:'center',backgroundColor:Setting.f9,bordercolor:'#ccc'}}>
                                            <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center',flex:1,padding:10}}>
                                                <View>
                                                    <FaText marquee style={{color:Setting.primaryColor}}>{this.type(item)} - {item.titleName}</FaText>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                )}
                            }
                            keyExtractor={(item)=>item.id}
                        />
                        </View>
                    </View>


                </Screen>
            )

    }
    remove(){
        if(this.state.selected.length == 0)
            return toastMsg("لطفا حداقل یک مورد را انتخاب کنید");

        alertMsg("آیا مطمين به حذف موارد انتخاب شده هستید ؟",'',
            [
                {text: 'لغو', onPress: () => {}},
                {text: 'حذف', onPress: () => {
                        this.setState({loading:true});
                        Fetcher("addFav",{id:this.state.selected},true)
                            .then((res)=>{
                                this.loadData();
                                this.setState({showRemove:false})
                            });

                    }},
            ],
            {cancelable: false}
        );


    }
    inArray = function(arr,comparer) {
        for(var i=0; i < arr.length; i++) {
            if(arr[i] == comparer) return true;
        }
        return false;
    };

    select(id){
        let selected = this.state.selected;
        if(id == 'all'){
            if(this.state.selected.length == this.state.courses.length){
                this.state.courses.map((val, i) => {
                    if (this.inArray(selected, val.id))
                        selected.splice( selected.indexOf(val.id), 1 );
                })
            }else {
                this.state.courses.map((val, i) => {
                    if (!this.inArray(selected, val.id))
                        selected.push(val.id);
                })
            }
        }else {
            if(this.inArray(selected,id)){
                selected.splice( selected.indexOf(id), 1 );
            }else {
                selected.push(id);
            }
        }
        this.setState({
            selected : selected,
        })

    }

}
