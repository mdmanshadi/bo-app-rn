import React from 'react';
import {View, Image, Dimensions, TouchableOpacity, FlatList, Platform, ImageBackground} from 'react-native';
import FaText from "./components/FaText";
import {Fetcher} from "./components/Upload";
import Loading from "./components/Loading";
import Screen from "./components/Screen";
import {Actions} from "react-native-router-flux";
import {Setting} from "./components/Setting";
import {alertMsg, toastMsg} from "./components/Helper";
import {Icon} from "native-base";
import {setStore} from "trim-redux";

export default class Message extends React.Component {


    componentWillMount(){
        this.setState({
            data : [],
            selected : [],
            loading : false,
            showRemove:false,
        });

        this.getData();
    }
    async getData(){
        var data = await Fetcher("userMessageHistory",{},true);
        if(data.status == "OK"){
            this.setState({
                data : data.data,
                loading : false,
            });
            setStore({
                unreadMsg : 0,
            });
        }else{
            this.setState({loading:false});
            setTimeout(()=>Actions.pop(),100);
        }
    }

    render() {
        var {height, width} = Dimensions.get('window');

        if(this.state.data.length == 0)
            return(
                <Screen float={true}  alignItems={'center'} title={"صندوق پیام"}>
                    <Loading new loading={this.state.loading}/>
                    <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:15,marginBottom:15,justifyContent:'center',alignItems:'center'}}>
                        <Image resizeMode={'stretch'} source={require('./../assets/images/bg/header.png')} style={{height:width * (179/500),width:width,marginBottom:30}} />
                        <View style={{backgroundColor:Setting.red,flexDirection:'row-reverse',alignItems:'center',justifyContent:'center',padding:10}}>
                            <Image source={require('../assets/images/drawer/error.png')} style={{width:30,height:30,marginLeft:8}}/>
                            <FaText style={{color:'#fff',fontSize:12}}>تاکنون پیامی نداشته اید.</FaText>
                        </View>

                    </View>
                </Screen>
            )
        else
            return(
                <Screen float={true}  alignItems={'center'} title={"صندوق پیام"}>
                    <Loading new loading={this.state.loading}/>
                    <View style={{alignItems:'center',justifyContent:'center'}}>
                        <ImageBackground resizeMode={'stretch'} source={require('./../assets/images/bg/header.png')} style={{height:width * (179/500),width:width,justifyContent:'flex-end',alignItems:'flex-start'}} >
                            {this.state.showRemove &&
                                <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center',margin:10,marginLeft:25}}>
                                    <FaText style={{marginLeft:5}}>انتخاب همه</FaText>
                                    <TouchableOpacity style={{width:15,height:15,borderWidth:1,borderColor:Setting.headerColor,justifyContent:'center',alignItems:'center',marginLeft:10}} onPress={()=>this.select('all')}>
                                        {this.state.selected.length == this.state.data.length &&
                                        <View style={{
                                            width: 13,
                                            height: 13,
                                            backgroundColor: Setting.headerColor
                                        }}/>
                                        }
                                    </TouchableOpacity>
                                    <FaText style={{marginLeft:5,marginRight:5}}>|</FaText>
                                    <TouchableOpacity onPress={()=>{
                                        this.remove();
                                    }} style={{marginRight:5,marginLeft:0}}>
                                        <Icon style={{fontSize:16}} name={"trash"} />
                                    </TouchableOpacity>
                                </View>
                            }
                        </ImageBackground>
                    </View>
                    <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:30,marginBottom:20,justifyContent:'center'}}>

                        <FlatList
                        data={this.state.data}
                        extraData={this.state}
                        renderItem={({item}) =>
                            <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center',width:'100%'}}>
                                {this.state.showRemove &&
                                    <TouchableOpacity style={{width:15,height:15,borderWidth:1,borderColor:Setting.headerColor,justifyContent:'center',alignItems:'center',marginLeft:10}} onPress={()=>this.select(item.id)}>
                                        {this.state.selected.indexOf(item.id) > -1 &&
                                        <View style={{
                                            width: 13,
                                            height: 13,
                                            backgroundColor: Setting.headerColor
                                        }}/>
                                        }
                                    </TouchableOpacity>
                                }
                                <TouchableOpacity onPress={()=>toastMsg(item.full_description)} onLongPress={()=>{this.select(item.id);this.setState({showRemove:true})}}  style={{borderColor:'#e9e9e9',borderRadius:0,margin:5,width:'80%',backgroundColor:'#fff',borderWidth:1}}>
                                    <View style={{padding:10,justifyContent:'center',alignItems:'center',backgroundColor:Setting.f9}}>
                                        <FaText style={{color:Setting.primaryColor}}>{item.created}</FaText>

                                        <FaText style={{marginTop:15}}>{item.title}</FaText>
                                        <FaText style={{marginTop:15}}>{item.description}</FaText>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        }
                        keyExtractor={item => item.id}
                    />
                    </View>
                </Screen>
            )
    }
    remove(){
        if(this.state.selected.length == 0)
            return toastMsg("لطفا حداقل یک مورد را انتخاب کنید");

        alertMsg("آیا مطمين به حذف موارد انتخاب شده هستید ؟",'',
            [
                {text: 'لغو', onPress: () => {}},
                {text: 'حذف', onPress: () => {
                        this.setState({loading:true});
                        Fetcher("deleteNotification",{id:this.state.selected},true)
                            .then((res)=>{
                                this.getData();
                                this.setState({showRemove:false})
                            });
                    }},
            ],
            {cancelable: false}
        );



    }
    inArray = function(arr,comparer) {
        for(var i=0; i < arr.length; i++) {
            if(arr[i] == comparer) return true;
        }
        return false;
    };
    select(id){
        let selected = this.state.selected;
        if(id == 'all'){
            if(this.state.selected.length == this.state.data.length){
                this.state.data.map((val, i) => {
                    if (this.inArray(selected, val.id))
                        selected.splice( selected.indexOf(val.id), 1 );
                })
            }else {
                this.state.data.map((val, i) => {
                    if (!this.inArray(selected, val.id))
                        selected.push(val.id);
                })
            }
        }else {
            if(this.inArray(selected,id)){
                selected.splice( selected.indexOf(id), 1 );
            }else {
                selected.push(id);
            }
        }
        this.setState({
            selected : selected,
        })

    }
}