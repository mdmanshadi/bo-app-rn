import React from 'react';
import {View, ScrollView, Dimensions,Image,FlatList,TouchableOpacity,Alert,AsyncStorage,ImageBackground} from 'react-native';
import {connect, getStore, setStore} from "trim-redux";
import FaText from "./components/FaText";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";
import MyButton from "./components/MyButton";
import {Actions} from "react-native-router-flux";
import TextField from "./components/TextField";
import Screen from "./components/Screen";
import ImagePicker from 'react-native-image-picker';
import {Setting} from "./components/Setting";
import {toastMsg} from "./components/Helper";
import Province from './../assets/Province';
import RNPickerSelect from 'react-native-picker-select';
import Pick from "./components/Pick";


export default class Profile extends React.Component {

    componentWillMount(){
        this.setState({
            loading:true,
            pic:'https://5eb3bde2e5e759001120478b.liara.space/oghyanooseabi-app/uploads/man.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=I36KNFQNR7SJ05V4665K1%2F20200607%2F%2Fs3%2Faws4_request&X-Amz-Date=20200607T103828Z&X-Amz-Expires=432000&X-Amz-SignedHeaders=host&X-Amz-Signature=94f678c71cdc001321ad72cde38ede9afb08203f761ebe8e6bcb38e11453c067',
            name: '',
            parent_name: '',
            b_y: '',
            b_m: '',
            b_d: '',
            melicode: '',
            country: '',
            state: '',
            city: '',
            address: '',
            zipcode: '',
            email: '',
            phone: '',
            pic_personal:'',
            id:'',
            stateList : [],
            cityList : [],
        });
        this.loadData();
        this.loadStateList();
    }
    loadStateList(){
        let List = [];
        Province.map((val)=>{
            List.push({ label: val.name, value: val.name });
        });
        this.setState({
            stateList : List,
        })
    }
    loadCityList(value){
        let List = [];
        let CityList = Province.find(x=>x.name == value);
        if(CityList)
        CityList.Cities.map((val)=>{
            List.push({ label: val.name, value: val.name });
        });
        this.setState({
            cityList : List,
        })
    }
    loadData(){
        Fetcher("getUserProfile",{},true)
            .then((res)=>{
                if(res.status == 'OK'){
                    this.setState({
                        name : res.data.name,
                        email: res.data.email,
                        phone: res.data.phone,
                        state: res.data.state,
                        city: res.data.city,
                        id : res.data.id,
                        pic:res.data.pic,
                        loading:false,
                    },()=>{
                        this.loadStateList();
                        this.setState({
                            country: res.data.country,
                        },()=>{
                            this.setState({
                                state: res.data.state,
                            },()=>{
                                this.loadCityList(res.data.state);
                                setTimeout(()=>{
                                    this.setState({
                                        city: res.data.city,
                                    })
                                },1500);
                            })
                        });
                    })
                }else{
                    Actions.reset("splash");
                }
            });
    }
    save(){
        this.setState({loading:true});
        Fetcher("saveUserProfile",{
            name : this.state.name,
            state:  this.state.state,
            city:  this.state.city,
            email:  this.state.email,
            phone:  this.state.phone,
        },true)
            .then((res)=>{
                this.setState({loading:false});
                if(res.status == 'OK'){
                    setTimeout(()=>toastMsg("اطلاعات با موفقیت ذخیره شد"),100);
                    Actions.reset("splash",{soft:true});
                }else{
                    toastMsg(res.data)
                }

            })
    }
    selectCameraImage(){
        const options = {
            title: 'انتخاب تصویر',
            takePhotoButtonTitle: 'گرفتن تصویر با دوربین',
            chooseFromLibraryButtonTitle: 'انتخاب عکس از گالری',
            cancelButtonTitle: 'انصراف',
            quality : 0.5,
            mediaType: 'photo',
            maxWidth:500,
            maxHeight:500,


            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.launchCamera(options, async (response) => {


            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                const source = { uri: response.uri };
                this.setState({loading:true});
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    pic: response.uri,
                    picURL: response.path,
                });

                var data = new FormData();

                data.append('avatar', {
                    uri: response.uri, // your file path string
                    name: 'avatar.jpg',
                    type: 'image/jpg'
                });

                Fetcher("ChangeAvatar",data,true,true)
                    .then(res=>{
                        this.setState({loading:false});
                        if(res.status == 'OK'){
                            toastMsg("تصویر پروفایل با موفقیت بارگزاری شد")
                        }else{
                            toastMsg("خطایی در بارگزاری تصویر پروفایل رخ داد")
                        }
                    });

            }
        });
    }
    selectGalleyImage(){
        const options = {
            title: 'انتخاب تصویر',
            takePhotoButtonTitle: 'گرفتن تصویر با دوربین',
            chooseFromLibraryButtonTitle: 'انتخاب عکس از گالری',
            cancelButtonTitle: 'انصراف',
            quality : 0.5,
            mediaType: 'photo',
            maxWidth:500,
            maxHeight:500,


            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.launchImageLibrary(options, async (response) => {


            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                console.log("response",response);

                const source = { uri: response.uri };
                this.setState({loading:true});
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    pic: response.uri,
                    picURL: response.path,
                });


                var data = new FormData();

                data.append('avatar', {
                    uri: response.uri, // your file path string
                    name: 'avatar.jpg',
                    type: 'image/jpg'
                });

                Fetcher("ChangeAvatar",data,true,true)
                    .then(res=>{
                        this.setState({loading:false});
                        if(res.status == 'OK'){
                            toastMsg("تصویر پروفایل با موفقیت بارگزاری شد")
                        }else{
                            toastMsg("خطایی در بارگزاری تصویر پروفایل رخ داد")
                        }
                    });
            }
        });
    }
    selectGalleyPersonalImage(){
        const options = {
            title: 'انتخاب تصویر',
            takePhotoButtonTitle: 'گرفتن تصویر با دوربین',
            chooseFromLibraryButtonTitle: 'انتخاب عکس از گالری',
            cancelButtonTitle: 'انصراف',
            quality : 0.5,
            mediaType: 'photo',
            maxWidth:500,
            maxHeight:500,


            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };
        ImagePicker.launchImageLibrary(options, async (response) => {


            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                const source = { uri: response.uri };
                this.setState({loading:true});
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    pic_personal: response.uri,
                    pic_personalURL: response.path,
                });


                var data = new FormData();

                data.append('avatar', {
                    uri: response.uri, // your file path string
                    name: 'avatar.jpg',
                    type: 'image/jpg'
                });

                Fetcher("ChangePersonalPic",data,true,true)
                    .then(res=>{
                        this.setState({loading:false});
                        if(res.status == 'OK'){
                        }else{
                        }
                    });

            }
        });
    }
    render() {
        var {height, width} = Dimensions.get('window');
        return(
            <Screen float={true} home>
                <Loading new loading={this.state.loading} title={"پروفایل من"}/>
                <View style={{marginRight:Setting.contentMargin,marginLeft:Setting.contentMargin,marginTop:15,marginBottom:15,justifyContent:'center',alignItems:'center'}}>
                    <ImageBackground resizeMode={'stretch'} source={require('../assets/images/bg/profile.png')} style={{height:width/1.432,width:width,justifyContent:'center',alignItems:'center',flexDirection:'column',marginBottom:15}} >

                        <View style={{justifyContent:'center',alignItems:'center',flexDirection:'row-reverse'}}>
                            <TouchableOpacity activeOpacity={0.8} onPress={()=>this.selectCameraImage()}>
                                <Image resizeMode={"cover"} source={require('./../assets/images/bg/camera.png')} style={{height:40,width:40,margin:10,borderRadius:20}} />
                            </TouchableOpacity>

                            <TouchableOpacity activeOpacity={1}>
                                <Image resizeMode={"cover"} source={{uri:this.state.pic}} style={{height:120,width:120,margin:10,borderRadius:60}} />
                            </TouchableOpacity>

                            <TouchableOpacity activeOpacity={0.8} onPress={()=>this.selectGalleyImage()}>
                                <Image resizeMode={"cover"} source={require('./../assets/images/bg/gallery.png')} style={{height:40,width:40,margin:10,borderRadius:20}} />
                            </TouchableOpacity>
                        </View>
                        <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
                            <FaText style={{fontSize:15}}>کد معرف : {this.state.id}</FaText>
                        </View>

                    </ImageBackground>

                    <TextField value={this.state.name} onChangeText={(value)=>this.setState({name:value})} placeholder={"نام و نام خانوادگی "}/>
                    <View style={{flexDirection:'row-reverse',width:'100%',justifyContent:'space-between'}}>
                        <View style={{width:'49%'}}>
                            <Pick
                                onValueChange={(value,label) => {if(value != 0) {this.setState({state:value});this.loadCityList(value)}}}
                                items={this.state.stateList}
                                placeholder={'استان'}
                                itemKey={this.state.state}
                                value={this.state.state}
                            />
                        </View>
                        <View style={{width:'49%'}}>
                            <Pick
                                onValueChange={(value,label) => {if(value != 0) this.setState({city:value})}}
                                items={this.state.cityList}
                                placeholder={'شهر'}
                                itemKey={this.state.city}
                                value={this.state.city}
                            />
                        </View>
                    </View>
                    <TextField value={this.state.email}  onChangeText={(value)=>this.setState({email:value})} placeholder={"پست الکترونیک"}/>
                    <TextField value={this.state.phone} keyboardType={"numeric"} onChangeText={(value)=>this.setState({phone:value})} maxLength={12} placeholder={"شماره تلفن ثابت"}/>

                    <MyButton onPress={()=>this.save()}>ذخیره اطلاعات</MyButton>
                </View>
            </Screen>
        )

    }

}
