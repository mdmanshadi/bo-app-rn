import React from 'react';
import {View, ScrollView, Dimensions, Image, FlatList, TouchableOpacity, Alert, AppState,ImageBackground} from 'react-native';
import {connect, getStore} from "trim-redux";
import FaText from "./components/FaText";
import {Setting,numberFormat} from "./components/Setting";
import Loading from "./components/Loading";
import {Fetcher} from "./components/Upload";
import MyButton from "./components/MyButton";
import Communications from "react-native-communications";
import TextField from "./components/TextField";
import Screen from "./components/Screen";
import MyButtonSelect from "./components/MyButtonSelect";
import CountDown from 'react-native-countdown-component';
import {Actions} from "react-native-router-flux";
import {toastMsg} from "./components/Helper";


class Quiz extends React.Component {

    componentWillMount(){
        AppState.addEventListener('change', this._handleAppStateChange);
        this.setState({
            loading:true,
            data : {},
            result : false,
            selected : [],
            details : this.props.data.details,
            questions : this.props.data.questions,
            question : 0,
            truescore : 0,
            totalscore : 0,
            remainTime : parseInt(this.props.data.details.exam_time),
        },()=>{
            setTimeout(()=>this.syncData(),200);
        });
    }

    componentWillUnmount(){
        AppState.removeEventListener('change', this._handleAppStateChange);
    }
    _handleAppStateChange = async (nextAppState) => {
        console.log(nextAppState);
        if (nextAppState == 'active'){
            this.syncData(true);
        }
    };
    syncData(selected = false){
        Fetcher("getExamDetails",{quiz_id:this.state.details.quiz_id},true)
            .then((res)=>{
                setTimeout(()=>this.setState({loading:false}),300);
                if(res.status == 'OK'){
                    if(selected){
                        this.setState({
                            remainTime : parseInt(res.data.remainTime),
                        })
                    }else{
                        this.setState({
                            remainTime : parseInt(res.data.remainTime),
                            selected : res.data.selected,
                        })
                    }

                }else{
                    toastMsg(res.data);
                }
            });

    }
    finishExam(){
        return Actions.replace('QuizResult',{id:this.state.details.quiz_id});


    }
    syncAnswer(id,val){
        Fetcher("setQuizAnswer",{quiz_id:this.state.details.quiz_id,question_id:id,value:val},true).then((res)=>{
            if(res.data == 'TIME_EXCEEDED'){
                this.finishExam();
            }
        });
    }
    nextQuestion(id,val){
        this.syncAnswer(id,val);
        if(this.state.question < (this.state.questions.length -1)){

            this.setState({
                question : this.state.question + 1,
            })
        }else{
            this.finishExam();
        }
    }
    prevQuestion(id,val){
        if(this.state.question > 0){
            this.syncAnswer(id,val);
            this.setState({
                question : this.state.question - 1,
            })
        }else{
            //("آزمون شروغ شد");
        }
    }
    selectAnswer(id,vid){
        var answers = this.state.selected;
        answers[id] = vid;
        this.setState({
            selected : answers,
        })
    }
    render() {
        var {height, width} = Dimensions.get('window');
        var question = this.state.questions[this.state.question] || [];
        var selected = this.state.selected[question.id];
        var answers = question.answers || [];

        return(

            <Screen title={"شرکت در آزمون"} float nomenu home>

                <Loading loading={this.state.loading}/>
                {(!this.state.result) ?
                    <View style={{marginRight:5,marginLeft:5,marginTop:5,marginBottom:20,justifyContent:'center',alignItems:'center'}}>
                        <ImageBackground resizeMode={'stretch'} source={require('./../assets/images/bg/header.png')} style={{height:width * (179/500),width:width,flexDirection:'row-reverse',justifyContent:'space-between',alignItems:'center'}}>
                            <FaText style={{color:Setting.headerColor,fontSize:12,padding:10,borderRadius:10,marginTop:(width * (179/500)) - 40}}>سوال {this.state.question + 1} از {this.state.questions.length}</FaText>

                        </ImageBackground>
                        <FaText style={{marginTop:10,color:Setting.red,fontSize:16,margin:5}} >{question.title}</FaText>
                        <View style={{margin:8,borderColor:Setting.white,borderWidth:1,padding:15,width:'90%',borderRadius:5,flexDirection:'column'}}>

                            <View style={{height:height/4,backgroundColor:Setting.f9,padding:15,marginBottom:20}}>
                                {answers.map((val,i)=>{
                                    return(
                                        <MyButtonSelect style={{marginTop:8}} onPress={()=>{this.selectAnswer(question.id,val.id)}} checked={selected == val.id}>{val.value}</MyButtonSelect>
                                    )
                                })}
                            </View>
                            <CountDown
                                until={this.state.remainTime * 60}
                                size={10}
                                onFinish={() => this.finishExam()}
                                digitStyle={{backgroundColor: Setting.f9,margin:10}}
                                digitTxtStyle={{color: Setting.red,fontSize:12}}
                                timeToShow={['M', 'S']}
                                timeLabels={{m: 'دقیقه', s: 'ثانیه'}}

                            />
                            <View style={{flex:1}}>
                                <View style={{justifyContent:'space-between',alignItems:'center'}}>
                                    {this.state.question == (this.state.questions.length -1) ?
                                        <MyButton onPress={()=>this.nextQuestion(question.id,selected)} style={{marginTop:20,width:'100%'}}>پایان آزمون</MyButton>
                                        :
                                        <MyButton onPress={()=>this.nextQuestion(question.id,selected)} style={{marginTop:20,width:'100%'}}>سوال بعدی</MyButton>
                                    }
                                    {this.state.question != 0 ?
                                        <MyButton onPress={() => this.prevQuestion()} style={{marginTop: 10,width:'100%'}}>سوال قبلی</MyButton>
                                        :
                                        <MyButton disable style={{marginTop: 10,width:'100%'}}>سوال قبلی</MyButton>

                                    }

                                </View>
                            </View>
                        </View>
                    </View>
                    :
                    <View style={{marginRight:5,marginLeft:5,marginTop:5,marginBottom:20,justifyContent:'center',alignItems:'center'}}>
                        <Image resizeMode={'stretch'} source={require('./../assets/images/bg/header.png')} style={{height:width * (179/500),width:width}} />

                        <FaText border>نتیجه آزمون</FaText>
                        <FaText style={{marginTop:15,fontSize:15}}>آزمون شما با موفقیت به پایان رسید</FaText>
                        <FaText style={{marginTop:15,fontSize:10}}>{getStore('config').exam_end_text}</FaText>

                        <View style={{margin:8,borderColor:Setting.white,borderWidth:1,padding:20,width:'90%',borderRadius:5,flexDirection:'column'}}>

                            <View style={{width:'100%',backgroundColor:Setting.f9,padding:10}}>
                                <FaText style={{marginBottom:15,fontSize:12}}>تعداد سوالات صحیح : {this.state.truescore} امتیاز</FaText>
                                <FaText style={{marginBottom:15,fontSize:12}}>تعداد کل سوالات : {this.state.totalscore} امتیاز</FaText>
                                <FaText style={{marginBottom:15,fontSize:12}}>نمره دریافتی شما از این آزمون : {this.state.score} امتیاز</FaText>
                                <FaText style={{marginBottom:15,fontSize:12}}>حداقل امتیاز برای قبولی : {this.state.details.min_score} امتیاز</FaText>


                            </View>
                            <View style={{width:'100%',backgroundColor:Setting.white}}>

                                {(parseInt(this.state.score) >= parseInt(this.state.details.min_score)) ?
                                    <FaText textAlign={'center'} style={{marginBottom:15,color:Setting.green,fontSize:25}}>وضعیت :  قبول</FaText>
                                    :
                                    <FaText textAlign={'center'} style={{marginBottom:15,color:Setting.red,fontSize:25}}>وضعیت : مردود</FaText>
                                }

                                <FaText textAlign={'center'} style={{marginBottom:15}}>میتوانید گزارش آزمون های خود را در منوی آزمون های من مشاهده کنید و در صورت قبولی مدرک قبولی خود را دریافت کنید.</FaText>
                                <MyButton onPress={()=>Actions.reset("splash",{soft:true})} style={{marginTop:30,width:'100%'}}>بازگشت</MyButton>
                            </View>
                        </View>
                    </View>
                }
            </Screen>
        )

    }

}
const mapStateToProps = (state) => {
    return {
        homeData : state.homeData

    }
};
export default connect(mapStateToProps)(Quiz);

