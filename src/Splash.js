import React from 'react';
import {Image, View, PushNotificationIOS, AsyncStorage, StatusBar, Platform, Dimensions} from 'react-native';
import {Spinner} from 'native-base';
import {Actions} from 'react-native-router-flux';
import FaText from "./components/FaText";
import {Fetcher} from "./components/Upload";
import {Setting} from "./components/Setting";
import {setStore} from "trim-redux";
import LinearGradient from 'react-native-linear-gradient';



export default class Splash extends React.Component {

    componentDidMount() {
        // SplashScreen.close({
        //     animationType: SplashScreen.animationType.scale,
        //     duration: 850,
        //     delay: 500,
        // })


    }
    componentWillMount(){
        this.loadData();
        if(Platform.OS != 'ios') {
            StatusBar.setBackgroundColor('#393939');
            StatusBar.setBarStyle('light-content');
        }else{
            StatusBar.setBarStyle('dark-content');
        }
    }
    async loadData(){
        Fetcher("getConfig", {})
            .then((res)=>{
                if (res.status == 'OK') {
                    setStore("config",res.data);
                }
            });
        console.log("loadData");
        Fetcher("GetMain", {}, true)
            .then((res) => {

                if (res.status == 'OK') {
                    setStore({
                        homeData : res.data,
                        unreadMsg : parseInt(res.data.user.unread_msg),
                    });

                    Actions.reset("main");
                } else {
                    AsyncStorage.removeItem("token")
                        .then(() => {
                            Actions.reset("login");
                        });
                }
            });

    }
    render() {
        var {height, width} = Dimensions.get('window');

        if(this.props.soft === undefined)
            return (
                <LinearGradient colors={['#fff', '#fff']} style={{flex:1,alignItems:'center'}}>
                    <View style={{marginTop:'15%'}}>
                        <Image resizeMode={'stretch'} style={{ width: width,height:width/2.024}} source={require('./../assets/images/logo_two.png')}/>
                    </View>
                    <View style={{position: 'absolute',bottom:height/10,alignItems:'center'}}>
                        <Image resizeMode={'contain'} style={{width: 50,height:50}} source={require('./../assets/images/loading.gif')}/>

                        <FaText style={{fontSize:10,color:Setting.primaryColor,marginTop:height/12}}>نسخه 1.0.0</FaText>
                    </View>
                </LinearGradient>
            );
        else
            return (
                <LinearGradient colors={['#fff', '#fff']} style={{flex:1,alignItems:'center'}}>
                    <View style={{position: 'absolute',bottom:15,alignItems:'center',marginTop:'60%'}}>
                        <Spinner color="black"  />
                    </View>
                </LinearGradient>
            );

    }


}