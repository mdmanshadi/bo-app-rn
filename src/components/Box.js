import React, { Component } from 'react';
import {View,Text,Image,TouchableOpacity,ImageBackground} from 'react-native';
import FaText from "./FaText";
import MarqueeText from 'react-native-marquee';
import FaTextMarqueeText from "./FaTextMarqueeText";
import {numberFormat, Setting} from "./Setting";
import {Icon} from "native-base";
import {Actions} from "react-native-router-flux";
import {getStore} from "trim-redux";

export default class Box extends Component {
    constructor(props) {
        super(props)
    }
    goto(){
        if(this.props.action){
            if (this.props.package) {
                return Actions.Package({id: this.props.item.id});
            } else {
                return Actions.Course({id: this.props.item.id});
            }
        }else {
            if (this.props.package) {
                return Actions.replace("Package", {id: this.props.item.id});
            } else {
                return Actions.replace("Course", {id: this.props.item.id});
            }
        }
    }
    render() {

        return (
            <TouchableOpacity  activeOpacity={1} onPress={()=>this.goto()} style={{
                width:180,
                height:240,
                backgroundColor:Setting.headerColor,
                marginRight:(this.props.nomargin ? 0: 8),
                marginLeft:(this.props.nomargin ? 0: 8),
                marginBottom:30,
                marginTop:10,
                borderRadius:0,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.23,
                shadowRadius: 0.62,
                elevation: 2,

            }}>

                <Image resizeMode={getStore('config').image_scale} style={{width:180,height:165,backgroundColor:'#fff'}} source={{uri:this.props.item.pic}} />
                {parseFloat(this.props.item.discount) > 0 &&
                    <ImageBackground resizeMode={'stretch'} style={{width:40,height:60,top:0,left:10,position:'absolute',justifyContent:'flex-end',alignItems:'center'}} source={require('../../assets/images/discountbg.png')} >
                        <FaText style={{bottom:5,color:Setting.white,fontSize:14}}>{Math.round(100 - ((parseFloat(this.props.item.price) - parseFloat(this.props.item.discount)) * 100) / parseFloat(this.props.item.price))}%</FaText>
                    </ImageBackground>
                }

                <View style={{backgroundColor:'rgba(255,255,255,0.8)',top:-29,padding:5}}>
                    <FaText style={{color:Setting.headerColor}} textAlign={'center'}>
                        {this.props.item.title}
                    </FaText>
                </View>

                <View style={{padding:10,top:-29,justifyContent:'center',alignItems:'center'}}>
                    <View style={{padding:5,flexDirection:'row-reverse',justifyContent:'space-between',alignItems:'space-between'}}>
                        {parseFloat(this.props.item.discount) > 0 ?
                            <FaText style={{color: '#b3915b', width: '49%',textDecorationLine: 'line-through'}} textAlign={'right'}>
                                {numberFormat(this.props.item.price)}
                            </FaText>
                            :
                            <View style={{color: Setting.white, width: '49%'}}></View>
                        }
                        <FaText style={{color:Setting.white,width:'49%'}} textAlign={'left'}>
                            {numberFormat(parseFloat(this.props.item.price) - parseFloat(this.props.item.discount))}
                        </FaText>
                    </View>

                    <View
                        style={{
                            width:50,
                            height:50,
                            backgroundColor:'#f1f2f2',
                            borderRadius:30,
                            borderWidth:1,
                            borderColor:'#f1f2f2',
                            justifyContent:'center',
                            alignItems:'center',
                            bottom:0,
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.62,
                            elevation: 4,
                        }}>
                        <Image source={require('./../../assets/images/shoppingcart.png')} style={{width:30}} resizeMode={"contain"}/>
                </View>


                </View>
            </TouchableOpacity>

        );
    }
}

