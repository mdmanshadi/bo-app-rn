export class DialogHelper {
    static Dialog;
    static onClose;

    static setDialog(Dialog) {
        this.Dialog = Dialog;
    }

    static show(msg, btns) {
        if (this.Dialog) {
            this.Dialog.show(msg,btns);
        }
    }
}