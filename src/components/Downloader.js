import React, { Component } from 'react';
import {View, Modal, Dimensions, Image, TouchableOpacity,TouchableWithoutFeedback} from 'react-native';
import {Spinner} from 'native-base';
import {Setting} from "./Setting";
import LinearGradient from 'react-native-linear-gradient';
import FaText from "./FaText";


export default class Dialog extends Component {
    constructor(props){
        super(props);
        this.state = {
            show : false,
            text : '',
            buttons : [],
        }
    }
    show(text ,buttons = [{text : 'بستن', onPress : ()=>{},}]){
        this.setState({
            show:true,
            text:text,
            buttons : buttons
        })
    }
    hide(callback){
        this.setState({
            show:false,
        });
        if(callback)
            callback();
    }

    render() {
        let {height, width} = Dimensions.get('window');
            return (
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.show}>

                    <TouchableOpacity activeOpacity={1} onPress={()=>this.hide()} style={{width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,0.6)'}}>
                        <View style={{width:'100%',height:'100%',alignItems:'center',justifyContent:'center'}}>
                            <TouchableWithoutFeedback>
                                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={[ '#f06b3d','#f35e66']} style={{width:'75%',alignItems:'center',alignContent:'center',backgroundColor:'red',justifyContent:'center',borderRadius:3}}>
                                    <View style={{flexDirection:'row-reverse',width:'98%',padding:10,justifyContent:'flex-start',alignItems:'flex-start'}}>
                                        <Image resizeMode={'stretch'} style={{width:50,height:50}} source={require('./../../assets/images/warning.png')} />
                                        <View style={{padding:5,paddingTop:15,width:'80%'}}>
                                            <FaText textAlign={"right"} style={{color:Setting.white}}>
                                                {this.state.text}
                                            </FaText>
                                        </View>
                                    </View>
                                    <View style={{flexDirection:'row-reverse',justifyContent:'center',alignItems:'center'}}>
                                        {this.state.buttons.map((button,i)=>{
                                            return(
                                                <TouchableOpacity onPress={()=>this.hide(button.onPress)} style={{width:'49%',padding:10,justifyContent:'center',alignItems:'center'}}>
                                                    <FaText style={{color:Setting.white}} textAlign={"center"}>{button.text}</FaText>
                                                </TouchableOpacity>
                                            )
                                        })}

                                    </View>
                                </LinearGradient>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableOpacity>

                </Modal>

            )
    }
}