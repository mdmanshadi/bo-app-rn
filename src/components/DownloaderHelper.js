export class DownloaderHelper {
    static Downloader;

    static setDownloader(Dialog) {
        this.Downloader = Dialog;
    }

    static async start(url,l_path,file_id) {
        if (this.Downloader) {
            this.Downloader.start(url,l_path,file_id);
        }
    }
}