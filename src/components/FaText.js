import React, { Component } from 'react';
import {Dimensions, Text, View} from 'react-native';
import FaTextMarqueeText from "./FaTextMarqueeText";
import {Setting} from "./Setting";
import HTML from 'react-native-render-html';

export default class FaText extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const style ={
            fontFamily:'IRANSansWeb(FaNum)',
            fontSize:(this.props.fontSize === undefined ? 11 : this.props.fontSize),
            textAlign:this.props.textAlign,
            padding:this.props.padding,
            writingDirection:'rtl',
            color:'#394b7f',


        }
        if(this.props.html !== undefined)
            return(<HTML ignoredStyles={['font-family','line-height','margin-top','margin-bottom']} containerStyle={[style,this.props.style]}  html={this.props.children} baseFontStyle={{fontFamily:'IRANSansWeb(FaNum)',writingDirection:'rtl',fontSize:(this.props.fontSize === undefined ? 14 : this.props.fontSize),color:(this.props.color === undefined ? '#373537' : this.props.color)}} imagesMaxWidth={Dimensions.get('window').width} />)

        else if(this.props.marquee === undefined)
            if(this.props.border === undefined)
                return (
                    <Text
                        style={[style,this.props.style]}>
                        {this.props.children}
                    </Text>
                );
            else
                return (
                    <View style={{paddingBottom:10,borderColor:Setting.primaryColor,borderBottomWidth:3}}>
                        <Text
                            style={[style,this.props.style]}>
                            {this.props.children}
                        </Text>
                    </View>

                );

        else
            return (
                <FaTextMarqueeText
                    marqueeOnStart
                    loop
                    style={[style,this.props.style]}
                >
                    {this.props.children}
                </FaTextMarqueeText>

            );
    }
}

