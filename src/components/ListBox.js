import React, { Component } from 'react';
import {View, Text, Image, TouchableOpacity, Dimensions} from 'react-native';
import FaText from "./FaText";
import MarqueeText from 'react-native-marquee';
import FaTextMarqueeText from "./FaTextMarqueeText";
import {numberFormat, Setting} from "./Setting";
import {Icon} from "native-base";
import {Actions} from "react-native-router-flux";
import MyButton from "./MyButton";
import {getStore} from "trim-redux";

export default class ListBox extends Component {
    constructor(props) {
        super(props)
    }
    goto(){
        console.log(this.props.package);
        if(this.props.action){
            if (this.props.package) {
                return Actions.Package({id: this.props.item.id});
            } else {
                return Actions.Course({id: this.props.item.id});
            }
        }else {
            if (this.props.package) {
                return Actions.replace("Package", {id: this.props.item.id});
            } else {
                return Actions.replace("Course", {id: this.props.item.id});
            }
        }
    }
    render() {
        var {height, width} = Dimensions.get('window');

        return (
            <TouchableOpacity  activeOpacity={1} onPress={()=>this.goto()} style={{
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.23,
                shadowRadius: 2.62,

                elevation: 4,
                width:width-70,
                flexDirection:'row-reverse',height:120,backgroundColor:'rgb(248,248,248)',marginRight:(this.props.nomargin ? 0: 15),marginLeft:(this.props.nomargin ? 0: 15),marginBottom:10,
                marginTop:10}}>
                <Image resizeMode={getStore('config').image_scale} style={{width:120,height:120}} source={{uri:this.props.item.pic}} />
                <View style={{padding:15,borderRightWidth:1,borderRightColor:'#ccc',justifyContent:'center',alignItems:'center',flex:1}}>
                    <FaText marquee={true} style={{marginTop:15,color:Setting.headerColor}}>
                        {this.props.item.title}
                    </FaText>
                    <FaText style={{marginTop:15,color:Setting.headerColor}}>
                        {numberFormat(this.props.item.price)}
                    </FaText>
                    <MyButton onPress={()=>this.goto()} slim style={{marginTop:15,color:Setting.headerColor}}><Icon style={{color:'#fff',fontSize:20}} name={'shopping-cart'} type={"FontAwesome"} /></MyButton>

                </View>
            </TouchableOpacity>

        );
    }
}

