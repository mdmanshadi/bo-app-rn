import React, { Component } from 'react';
import {View, Modal, Dimensions} from 'react-native';
import {Spinner} from 'native-base';
import {Setting} from "./Setting";


export default class Loading extends Component {
    render() {
        var {height, width} = Dimensions.get('window');
        if(this.props.new)
            return (
            <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.props.loading === undefined ? false:this.props.loading}
                >
                <View style={{marginTop: (height/3)*2,alignItems:'center'}}>
                    <View>
                        <View style={{width:120,height:120,alignItems:'center',alignContent:'center',backgroundColor:'rgba(0,0,0,0)',justifyContent:'center',borderRadius:15}}>
                            <Spinner color="black" />
                        </View>
                    </View>
                </View>
            </Modal>
            );
        else
            return (
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.props.loading === undefined ? false:this.props.loading}>

                    <View style={{marginTop: height/2-60,alignItems:'center'}}>
                        <View>
                            <View style={{width:120,height:120,alignItems:'center',alignContent:'center',backgroundColor:'rgba(0,0,0,0.6)',justifyContent:'center',borderRadius:15}}>
                                <Spinner color="white" />
                            </View>
                        </View>
                    </View>

                </Modal>

            )
    }
}