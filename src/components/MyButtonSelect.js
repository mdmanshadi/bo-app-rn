import React, { Component } from 'react';
import {View,TouchableOpacity} from 'react-native';
import FaText from "./FaText";
import {Setting} from "./Setting";
export default class MyButtonSelect extends Component {
    render() {
        return (
            <TouchableOpacity  activeOpacity={1} onPress={()=>{
                if(!this.props.disable)
                    return this.props.onPress();
            }} style={[{justifyContent:'flex-end',alignItems:'flex-end',width:'100%'},this.props.style]}>
            <View style={{flexDirection:'row-reverse',alignItems:'center',justifyContent:'center'}}>
                <View style={{borderRadius:40,borderWidth:1,width:20,height:20,borderColor:'#ccc',marginLeft:15,justifyContent:'center',alignItems:'center'}}>
                    {this.props.checked &&
                        <View style={{backgroundColor:Setting.headerColor,width:15,height:15,borderRadius:30,}}></View>
                    }
                </View>
                <FaText>{this.props.children}</FaText>
            </View>
            </TouchableOpacity>
        );
    }
}