import React, { Component } from 'react';
import { TouchableOpacity, View, Modal, Dimensions} from 'react-native';
import {StyleProvider, Container, Header, Content,Icon} from 'native-base';
import FaText from "./FaText";
import {Actions} from "react-native-router-flux";
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import Loading from "./Loading";

export default class ScreenFix extends Component {
    close(){
        if(this.props.iconAction === undefined)
            return Actions.popTo("home");
        else return this.props.iconAction(false);
    }
    render() {
        var {height, width} = Dimensions.get('window');


        return (
            <StyleProvider style={getTheme(material)}>
                <View style={{backgroundColor:'#fff',height:'100%'}}>
                    <Loading loading={this.props.loading} />
                    <Header style={{alignItems:'center',height:80}}>

                            <TouchableOpacity  activeOpacity={1} onPress={()=>this.close()} style={{position:'absolute',right:20,top:42}}>
                                <Icon name={"close"} style={{fontSize:25}} />
                            </TouchableOpacity>

                        <FaText textAlign={"right"}  color={"#000"}>{this.props.title}</FaText>
                    </Header>
                    <View style={{padding:this.props.padding === undefined ? 20 : this.props.padding,flex:1,height:'100%',backgroundColor:'#fff'}} contentContainerStyle={{alignItems:this.props.alignItems === undefined ? 'flex-end' : this.props.alignItems,backgroundColor:'#fff'}}>
                        {this.props.children}

                    </View>
                </View>
            </StyleProvider>
        );
    }
}