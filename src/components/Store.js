import {createStore} from 'trim-redux'
const state = {
    homeData : [],
    config : {},
    pass : {},
    unreadMsg : 0,
    download : null,
}
export const Store = createStore(state);