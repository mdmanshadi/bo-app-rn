import React, { Component } from 'react';
import { TextInput} from 'react-native';
import {Setting} from "./Setting";
export default class TextField extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        let padding = {
            paddingRight:(this.props.slim ? 5 :8),
            paddingLeft:(this.props.slim ? 5 :8),
            paddingTop:(this.props.slim ? 5 :8),
            paddingBottom:(this.props.slim ? 5 :8),
        };
        if(this.props.padding)
            padding = this.props.padding;

        const style = {
            width:'100%',
            marginTop:5,
            marginBottom:5,

            textAlign:'right',
            borderWidth:1,
            borderRadius:Setting.borderRadius,
            backgroundColor:(this.props.redbg ? '#f39584' :'#fff'),
            borderColor:(this.props.redbg ? '#f39584' :'#cacfe0'),
            // lineHeight:1,
            fontFamily:'IRANSansWeb(FaNum)',
            fontSize:(this.props.slim ? 9 :11),

        };
        return (
            <TextInput secureTextEntry={this.props.secureTextEntry}
                       defaultValue={this.props.defaultValue}
                       value={this.props.value}
                       textContentType={this.props.textContentType}
                       textAlign={'center'}
                       style={[style,this.props.style,padding]}
                       underlineColorAndroid = "transparent"
                       placeholder = {this.props.placeholder}
                       placeholderTextColor = {(this.props.redbg ? '#000' :'#9f9e9e')}
                       returnKeyType={this.props.returnKeyType}
                       onChangeText={this.props.onChangeText}
                       keyboardType={this.props.keyboardType}
                       focus = {this.props.focus}
                       onSubmitEditing = {this.props.onSubmitEditing}
                       multiline = {false}
                       editable = {this.props.editable}
                       numberOfLines = {1}
                       maxLength = {this.props.maxLength}

            />

        );
    }
}

